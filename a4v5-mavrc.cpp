/*========================================================================================================
											  ID Block
========================================================================================================*/
/*

Due Date:           November 20th, 2015
Software Designer:  Christo Mavrick
Course:             420-316-AB
Deliverable:        Assignment #4 --- States-Parsing

Description:        This is an exercise in state-analysis and state-variable application design.
                    Data is supplied as a 2-dimensional array of characters whose rows are traditional C-strings.
                    The data is just put through a finite state machine equipped with multiple state handlers to
                    properly handle every character in the array of strings. While going through this finite state
                    machine, data is recorded and put into respective arrays depending on what type of data the
                    current section of the string being examined is parsed to be (integer, double, or word), the
                    data is output at the end in a neat a nice format.

*/

/*========================================================================================================
											Preliminaries
========================================================================================================*/
#include <iostream>
#include <iomanip>             //for setw function to format output
#include <conio.h>			  //for printf function
#pragma warning (disable: 4996)
using namespace std;

/*========================================================================================================
										  Global Variables
========================================================================================================*/
const int LMAX = 200;		//maximum number of chars per line 
const int NLINES = 10;		//maximum number of lines
const int NDBLE = 10;		//maximum number of doubles
const int NINT = 10;		//maximum number of integers
const int WMAX = 16;		//maximum number of characters in a word
//array of text lines i.e. the input data
char tline[NLINES][LMAX] = {
	" first 123		and then -.1234 but you'll need 123.456	  	   and 7e-4 plus one like +321. all quite avant-",
	"garde   whereas ellen's true favourites are 123.654E-2  		an exponent-form which can also be -54321E-03 or this -.9E+5",
	"We'll prefer items like		fmt1-decimal		+.1234567e+05 or fmt2-dec -765.3245 or fmt1-int -837465 and vice-",
	"versa or even format2-integers -19283746 		  making one think of each state's behaviour for 9 or even 3471e-7 states ",
};
enum State{ white, word, num, dbl, expt };
enum CharType{ whsp, lett, expo, dig, iplus, iminus, point, apos, stop };
char line[LMAX];
int len;
State state;
CharType type;
// ARRAYS FOR RESULTS
double mydbls[NDBLE]; // Double results
int myints[NINT]; // Int Results
int mywords[WMAX];	//Word results
// OTHER VARIABLES
char ch; // Character currently being examined
int	k, // Current subscript of the line being put into Ch to be examined
	n, // Result of ASCII to Int conversion
	sign, // Sign used to determine if Int and Double values are positive or negative
	power, // Power used to properly determine decimal position in double values
	wordC, // Word count used to determine length of a given word
	esign, // Sign used to determine if exponent will be positive or negative
	doubleIndex = 0, // Used to track amount of doubles we've put into the double results array
	intIndex = 0; // Used to track amount of ints we've put into the int results array
double d, // Result of ASCII to Double conversion
	   e; // Used to determine what power/exponent is needed
bool hyphenOn; // Boolean switch to determine if the final character in a line was a hyphen

/*========================================================================================================
										   Function Prototypes
========================================================================================================*/
CharType getType();
void WhiteState();
void WhiteToWord();
void WhiteToNum();
void WhiteToDbl();
void WordState();
void WordToWhite();
void NumState();
void NumToWhite();
void NumToDbl();
void NumToExpo();
void DblState();
void DblToWhite();
void DblToExpo();
void ExpoState();
void ExpoToWhite();

/*========================================================================================================
											 Main Block
========================================================================================================*/
int main()
{	cout << "TEXT DATA LINES:" << endl << endl;
	hyphenOn = false;
	state = white;
	for (int j = 0; j < NLINES && strlen(tline[j]) > 0; j++)
	{	cout << tline[j] << endl;
		strcpy(line, tline[j]); // Copy the j'th line so that we can work with it
		len = strlen(line); // Get the length of said line
		k = 0; // Initialize subscript that will go through string "line"
		ch = line[k]; // Get the k'th character of the line, the character that we'll be dealing with
		type = getType(); // Get and set the type for said character
		while (k < len) // We have not reached the end of the line
		{	switch (state)
			{	case white: WhiteState(); break;
				case word: WordState(); break;
				case num: NumState(); break;
				case dbl: DblState(); break;
				case expt: ExpoState(); break;
			}
		}
		if (!hyphenOn) // The end of the previous line was not a hyphen, so we return to white state
		{	switch (state)
			{	case word: WordToWhite(); break;
				case num: NumToWhite(); break;
				case dbl: DblToWhite(); break;
				case expt: ExpoToWhite(); break;
			}
		}
	}
	// Being Printing Output
	printf("\n\nANALYSIS RESULTS\n");
	printf("----------------------------------------------\n\n");
	// Word Table
	// -----------------------------------------------------
	printf("WORD RESULTS\n%c", 201);
	for (int pj = 0; pj < 23; pj++)
		printf("%c", 205);
	printf("%c\n", 187);
	printf("%cWORD LENGTH   FREQUENCY%c\n", 186, 186);
	for (int wj = 0; wj < WMAX; wj++) //Word Table Loop
		if (mywords[wj] > 0)
		{	printf("%c", 204);
			for (int pj = 0; pj < 23; pj++)
				printf("%c", 205);
			printf("%c\n%c%d\t%16d%c\n", 185, 186, wj + 1, mywords[wj], 186);
		}
	printf("%c", 200);
	for (int pj = 0; pj < 23; pj++)
		printf("%c", 205);
	// Int Table
	// -----------------------------------------------------
	printf("%c\n\nINTEGER RESULTS\n%c", 188, 201);
	for (int pj = 0; pj < 28; pj++)
		printf("%c", 205);
	printf("%c\n", 187);
	printf("%cINDEX\t\t\tVALUE%c\n", 186, 186);
	for (int ij = 0; ij < intIndex; ij++) //Int Table Loop
	{	printf("%c", 204);
		for (int pj = 0; pj < 28; pj++)
			printf("%c", 205);
		printf("%c\n%c%d\t%21d%c\n", 185, 186, ij, myints[ij], 186);
	}
	printf("%c", 200);
	for (int pj = 0; pj < 28; pj++)
		printf("%c", 205);
	// Double Table
	// -----------------------------------------------------
	printf("%c\n\nDOUBLE RESULTS\n%c", 188, 201);
	for (int pj = 0; pj < 28; pj++)
		printf("%c", 205);
	printf("%c\n", 187);
	printf("%cINDEX\t\t\tVALUE%c\n", 186, 186);
	for (int dj = 0; dj < doubleIndex; dj++) //Double Table Loop
	{	printf("%c", 204);
		for (int pj = 0; pj < 28; pj++)
			printf("%c", 205);
		printf("%c\n%c%d\t%21.15g%c\n", 185, 186, dj, mydbls[dj], 186);
	}
	printf("%c", 200);
	for (int pj = 0; pj < 28; pj++)
		printf("%c", 205);
	printf("%c\n\n", 188);
	system("PAUSE");
	return 0;
}

CharType getType()
{	CharType t;
	if (isspace(ch)) // Spaces?
		t = whsp;
	else if (isdigit(ch)) // Digits?
		t = dig;
	else if (isalpha(ch)) // Letters?
	{	if (toupper(ch) == 'E') // Seperate E and e for possible expo state
			t = expo;
		else // All other letters
			t = lett;
	}
	else // Plus, Minus, Decimal, Apostrophe, and String Terminator
	{	switch (ch)
		{	case '+': t = iplus; break;
			case '-': t = iminus; break;
			case '.': t = point; break;
			case '\'': t = apos; break;
			case '\0': t = stop; break;
		}
	}
	return t;
}

void WhiteState()
{	while (state == white && k < len) // State remains white and we have not reached the end of the line
	{	switch (type)
		{	case expo:
			case lett:
				WhiteToWord(); break;
			case dig:
			case iplus:
			case iminus:
				WhiteToNum(); break;
			case point:
				WhiteToDbl(); break;
		}
		ch = line[++k]; // Increment k and get the following character
		type = getType(); // Get and set the type for said character
	}
}

void WhiteToWord()
{	state = word;
	wordC = 1; // Reset the word counter since we're starting a new word
}

void WhiteToNum()
{	state = num;
	if (ch == '-') // Number is negative
		sign = -1;
	else if (ch != '.') // Number is positive
		sign = 1;
	if (ch != '-' && ch != '+') // Ch is an int digit
		n = 10 * n + ch - '0'; // ASCII to Int conversion
}

void WhiteToDbl()
{	state = dbl;
	if (ch == '-') // Number is negative
		sign = -1;
	else if (ch != '.') // Number is positive
		sign = 1;
	d = 0;
	power = 1;
	if (ch != '-' && ch != '+' && ch != '.') // Ch is a double digit
	{	d = 10 * d + ch - '0'; // ASCII to Double conversion
		power *= 10; // Set power to later apply correct decimal position
	}
}

void WordState()
{	while (state == word && k < len) // State remains word and we have not reached the end of the line
	{	if (type == whsp)
			WordToWhite();
		else
			wordC++; // The word is 1 value longer
		if (type == iminus) // A hyphen was found
			hyphenOn = true;
		else // A hyphen was not found
			hyphenOn = false;
		ch = line[++k]; // Increment k and get the following character
		type = getType(); // Get and set the type for said character
	}
}

void WordToWhite()
{	mywords[wordC - 1]++; // Add 1 to the slot corresponding to words of the current word length
	state = white;
}

void NumState()
{	while (state == num && k < len) // State remains num and we have not reached the end of the line
	{	switch (type)
		{	case whsp:
				NumToWhite(); break;
			case point:
				NumToDbl(); break;
			case expo:
				NumToExpo(); break;
			default:
				n = 10 * n + ch - '0'; // ASCII to Int conversion
		}
		ch = line[++k]; // Increment k and get the following character
		type = getType(); // Get and set the type for said character
	}
}

void NumToWhite()
{	n = sign * n; // Assign our previously determined sign to our converted int number
	myints[intIndex++] = n; // Add our converted int number to the int array
	state = white;
	n = 0; // Re-set n to 0 for the next int number we may encounter
}

void NumToDbl()
{	state = dbl;
	if (ch == '-') // Number is negative
		sign = -1;
	else if (ch != '.') // Number is positive
		sign = 1;
	d = n; // Give d previous value of n since we came from num state
	power = 1;
	if (ch != '-' && ch != '+' && ch != '.') // Ch is a double digit
	{	d = 10 * d + ch - '0'; // ASCII to Double conversion
		power *= 10; // Set power to later apply correct decimal position
	}
}

void NumToExpo()
{	state = expt;
	e = 0;
	d = sign * n; // Save the previously determined sign that would otherwise be lost
}

void DblState()
{	while (state == dbl && k < len) // State remains dbl and we have not reached the end of the line
	{	switch (type)
		{	case whsp:
				DblToWhite(); break;
			case expo:
				DblToExpo(); break;
			default:
				d = 10 * d + ch - '0'; // ASCII to Double conversion
				power *= 10; // Set power to later apply correct decimal position
		}
		ch = line[++k]; // Increment k and get the following character
		type = getType(); // Get and set the type for said character
	}
}

void DblToWhite()
{	d = sign * d / power; // Assign our previously determined sign to our converted double number
	mydbls[doubleIndex++] = d; // Add our converted double number to the double array
	state = white;
	n = 0; // Re-set n to 0 for the next int number we may encounter
	d = 0; // Re-set d to 0 for the next double number we may encounter
}

void DblToExpo()
{	state = expt;
	d = sign * d / power; // Apply sign and power to d before going into expo state
	e = 0; // Set e to 0 so that the value is always accurate when going into expo state
}

void ExpoState()
{	while (state == expt && k < len) // State remains expt and we have not reached the end of the line
	{	if (ch == '+') // We need to apply a positive exponent
			esign = 1;
		else if (ch == '-') // We need to apply a negative exponent
			esign = -1;
		if (type == whsp) // Escape to white space because Ch is white space
			ExpoToWhite();
		else // Ch is not white space
			if (ch != '-' && ch != '+') // Ch is an exponent digit
				e = e * 10 + ch - '0'; // ASCII to Expo conversion
		ch = line[++k]; // Increment k and get the following character
		type = getType(); // Get and set the type for said character
	}
}

void ExpoToWhite()
{	if (esign == 1) // Exponent was positive
		while (e-- > 0) // Exponent is not 0, decrement exponent
			d *= 10;
	else // Exponent was negative
		while (e-- > 0) // Exponent is not 0, decrement exponent
			d /= 10;
	mydbls[doubleIndex++] = d; // Add our converted double number with exponent applied to the double array
	state = white;
	n = 0; // Re-set n to 0 for the next int number we may encounter
	d = 0; // Re-set d to 0 for the next double number we may encounter
}