This is an exercise in state-analysis and state-variable application design.

Data is supplied as a 2-dimensional array of characters whose rows are traditional C-strings.

The data is put through a finite state machine equipped with multiple state
handlers to properly handle every character in the array of strings.

While going through this finite state machine, data is recorded and put
into respective arrays depending on what type of data the current
section of the string being examined is parsed to be (integer, double, or word),
the data is then output at the end in a neat a nice format.